import {isNil, isEmpty, anyPass} from 'ramda'

export const isNullOrEmpty = anyPass([isNil, isEmpty])
