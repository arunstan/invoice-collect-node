import express from 'express'
import routeAccounts from './api/accounts'
import routeAccountsAuthorize from './api/accountsAuthorize'
import routeAccountsTokenExists from './api/accountsTokenExists'
import routeAccountsPassword from './api/accountsPassword'
import routeAccountsAccount from './api/accountsAccount'
import routeAccountsName from './api/accountsName'
import validateToken from './validateToken'

// get an instance of the router for api routes
const apiRoutes = express.Router()

apiRoutes.post('/accounts', (req, res) => {
  routeAccounts(req, res)
})

apiRoutes.post('/accounts/authorize', (req, res) => {
  routeAccountsAuthorize(req, res)
})

apiRoutes.post('/accounts/tokenexists', (req, res) => {
  routeAccountsTokenExists(req, res)
})

// route middleware to verify a token in all the further requests
apiRoutes.use((req, res, next) => {
  validateToken(req, res, next)
})

apiRoutes.put('/accounts/password', (req, res) => {
  routeAccountsPassword(req, res)
})

apiRoutes.get('/accounts/account', (req, res) => {
  routeAccountsAccount(req, res)
})

apiRoutes.put('/accounts/name', (req, res) => {
  routeAccountsName(req, res)
})

export default apiRoutes
