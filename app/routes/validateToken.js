import jwt from 'jsonwebtoken'
import {config} from '../../config'

function getEmailFromToken (decodedToken) {
  return decodedToken && decodedToken.payload && decodedToken.payload.email
}

export default (req, res, next) => {
  // check header or url parameters or post parameters for token
  const token = req.headers['authorization']
  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' })
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded
        req.userEmail = getEmailFromToken(decoded)
        next()
      }
    })
  } else {
    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    })
  }
}
