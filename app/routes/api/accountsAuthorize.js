import jwt from 'jsonwebtoken'
import User from '../../models/user'
import {config} from '../../../config'

export default (req, res) => {
  const {email, password} = req.body
  // find the user
  User.findOne({
    email: email
  }, (err, user) => {
    if (err) throw err

    if (!user) {
      res.json({ success: false, message: 'Authentication failed. User not found.' })
    } else if (user) {
      // check if password matches
      user.comparePassword(password, (err, isMatch) => {
        if (err) throw err

        if (isMatch) {
          // if user is found and password is right
          // create a token
          const {email, password} = user
          const token = jwt.sign({'payload': {email, password}}, config.secret, {
            expiresInMinutes: 1440 // expires in 24 hours
          })
          // return the information including token as JSON
          res.json({success: true, token: token})
        } else {
          res.json({ success: false, message: 'Authentication failed. Wrong password.' })
        }
      })
    }
  })
}
