import User from '../../models/user'

export default (req, res) => {
  const {email, password} = req.body
  // find the user
  User.findOne({
    email: email
  }, (err, user) => {
    if (err) throw err

    if (!user) {
      res.json({ success: false, message: 'Password change failed. User not found.' })
    } else if (user) {
      user.password = password
      user.save(err => {
        if (err) throw err
        res.json({ success: true, message: 'Password changed successfully' })
      })
    }
  })
}
