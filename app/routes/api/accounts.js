import {isNullOrEmpty} from '../../utils/tools'
import User from '../../models/user'

export default (req, res) => {
  const {firstName, lastName, email, password, dataPolicy} = req.body

  console.log(req)

  if (isNullOrEmpty(firstName)) {
    return res.json({ success: false, message: 'Error. First name is required' })
  }

  if (isNullOrEmpty(lastName)) {
    return res.json({ success: false, message: 'Error. Last name is required' })
  }

  if (isNullOrEmpty(email)) {
    return res.json({ success: false, message: 'Error. Email is required' })
  }

  if (isNullOrEmpty(password)) {
    return res.json({ success: false, message: 'Error. password is required' })
  }

  if (isNullOrEmpty(dataPolicy) && dataPolicy) {
    return res.json({ success: false, message: 'Data policy must be checked' })
  }

  User.findOne({
    email: email
  }, (err, user) => {
    if (err) throw err

    if (user) {
      return res.json({ success: false, message: 'User already exists!' })
    } else {
      const newUser = new User({firstName, lastName, email, password, dataPolicy})

      newUser.save(err => {
        if (err) throw err
        console.log('User saved successfully')
        return res.json({ success: true })
      })
    }
  })
}
