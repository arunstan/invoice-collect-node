import User from '../../models/user'

export default (req, res) => {
  const email = req.userEmail
  // find the user
  User.findOne({
    email: email
  }, (err, user) => {
    if (err) throw err
    if (user) {
      const {firstName, lastName, email} = user
      res.json({firstName, lastName, email})
    } else res.status(404).send()
  })
}
