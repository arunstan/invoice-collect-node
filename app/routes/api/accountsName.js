import User from '../../models/user'

export default (req, res) => {
  const email = req.userEmail
  const {firstName, lastName} = req.body

  if (firstName && lastName) {
    // find the user
    User.findOne({
      email: email
    }, (err, user) => {
      if (err) throw err

      if (!user) {
        res.status(400).send({ success: false, message: 'Name change failed. User not found.' })
      } else if (user) {
        user.firstName = firstName
        user.lastName = lastName
        user.save(err => {
          if (err) throw err
          res.status(200).send()
        })
      }
    })
  } else {
    res.status(400).send({ success: false, message: 'Name change failed. Invalid first name or last name' })
  }
}
