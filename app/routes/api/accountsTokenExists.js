import jwt from 'jsonwebtoken'
import {config} from '../../../config'

export default (req, res, next) => {
  const token = req.body.token
  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        decoded ? next(err) : res.status(404).send()
      } else {
        decoded ? res.status(200).send() : res.status(404).send()
      }
    })
  } else {
    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    })
  }
}
