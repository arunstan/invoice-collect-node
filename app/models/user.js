// get an instance of mongoose and mongoose.Schema
import mongoose from 'mongoose'
import bcrypt from 'bcrypt'
import {config} from '../../config'

const {Schema} = mongoose

const UserSchema = new Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  dataPolicy: Boolean
})

UserSchema.pre('save', function (next) {
  var user = this

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next()

    // generate a salt
  bcrypt.genSalt(config.saltWorkFactor, (err, salt) => {
    if (err) return next(err)

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) return next(err)

      // override the cleartext password with the hashed one
      user.password = hash
      next()
    })
  })
})

UserSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) return cb(err)
    cb(null, isMatch)
  })
}

export default mongoose.model('User', UserSchema)
