# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

A node.js backend for API's used in the Invoice & Collect app

### How do I get set up? ###

#### Database configuration ####

* Install [Mongo DB](https://docs.mongodb.org/manual/installation/#mongodb-community-edition)
* Start `mongod`

```
#!bash

mongod --dbpath <path-to-data-directory>
```

* Open another terminal
* Start `mongo` shell

```
#!bash

mongo
```
* Add an administrative user

```
#!Javascript

use admin
db.createUser(
  {
    user: "admin",
    pwd: "<admin-user-password>",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)
```

* Restart `mongod` instance enabling authentication 

```
#!Javascript

mongod --auth --port 27017 --dbpath <path-to-data-directory>
```

* Start `mongo` shell as the admin user

```
#!bash

mongo --port 27017 -u "admin" -p "<admin-user-password>" --authenticationDatabase "admin"
```

* Add user for invoice and collect app

```
#!Javascript

use invoiceCollect
db.createUser(
  {
    user: "ic",
    pwd: "<ic-admin-user-password>",
    roles: [ { role: "dbOwner", db: "invoiceCollect" } ]
  }
)
```
#### Update config files ####
Update the `config.js` file `database` property as per the above configuration

ie:
```
#!bash

'database': 'mongodb://ic:<ic-admin-user-password>@127.0.0.1:27017/invoiceCollect',

```

#### Install NPM modules ####

```
#!bash

npm install
```

#### Start the app ####

```
#!bash

npm start
```

Endpoint can be reached at http://127.0.0.1:7990/api