
// =======================
// get the packages we need ============
// =======================

import express from 'express'
import bodyParser from 'body-parser'
import morgan from 'morgan'
import mongoose from 'mongoose'
import cors from 'cors'

import {config} from './config' // get our config file
import apiRoutes from './app/routes/routes'

const app = express()

// =======================
// configuration =========
// =======================
const port = process.env.PORT || 7990 // used to create, sign, and verify tokens
mongoose.connect(config.database) // connect to database
app.set('jwtSecret', config.secret) // secret variable

app.use(cors())

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }))
// allows body parser to recognize crunch custom json requests content-type headers
app.use(bodyParser.json({ type: req => /^application\/[vnd\.crunch\S]*json$/.test(req.get('Content-Type')) }))

// use morgan to log requests to the console
app.use(morgan('dev'))

// =======================
// routes ================
// =======================
// apply the routes to our application with the prefix /api
app.use('/api', apiRoutes)

// =======================
// start the server ======
// =======================
app.listen(port)
console.log('App started at port:' + port)
